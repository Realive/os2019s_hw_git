#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>

std::vector<int> cylinderGenerator(int seed, int num) {
	std::vector<int> output;
	srand(seed);
	for (int i = 0; i < num; i++) {
		output.push_back(rand()%5000);
	}
	return output;
}

int fcfs(std::vector<int> cylinderRequest, int startPosition) {
	int total = 0;
	int start = startPosition;
	for(auto it = cylinderRequest.begin(); it != cylinderRequest.end(); ++it) {
		total += abs(start - (*it));
		start = (*it);
	}
	return total;
}

int sstf(std::vector<int> cylinderRequest, int startPosition) {
	int total = 0;
	int start = startPosition;
	for (int n = 0; n < cylinderRequest.size(); n++) {
		int next_pos = 0;
		for(int i = 0; i < cylinderRequest.size(); i++) {
			if (abs(start - cylinderRequest[i]) < abs(start - cylinderRequest[next_pos]))
				next_pos = i;
		}
		total += abs(start - cylinderRequest[next_pos]);
		start = cylinderRequest[next_pos];
		cylinderRequest.erase(cylinderRequest.begin() + next_pos);
		n--;
	}
	return total;
}

int scan(std::vector<int> cylinderRequest, int startPosition) {
	std::vector<int> sort_cylinder = cylinderRequest;
	std::sort(sort_cylinder.begin(), sort_cylinder.end());
	return startPosition + abs(sort_cylinder[sort_cylinder.size() - 1] - sort_cylinder[0]);
}

int cscan(std::vector<int> cylinderRequest, int startPosition) {
	std::vector<int> sort_cylinder = cylinderRequest;
	std::sort(sort_cylinder.begin(), sort_cylinder.end());
	int start = startPosition;
	int next_pos = 0;
	for (int i = 0; i < sort_cylinder.size() - 1; i++) {
		if (abs(start - sort_cylinder[i]) <= abs(start - sort_cylinder[next_pos]) && abs(start - sort_cylinder[next_pos]) < abs(start - sort_cylinder[i + 1]))
			next_pos = i + 1;
	}
	return abs(5000 - sort_cylinder[next_pos]) + (sort_cylinder[next_pos - 1]) + 5000;
}

int look(std::vector<int> cylinderRequest, int startPosition) {
	std::vector<int> sort_cylinder = cylinderRequest;
	std::sort(sort_cylinder.begin(), sort_cylinder.end());
	int start = startPosition;
	int total = 0;
	int next_pos = 0;
	for (int i = 0; i < sort_cylinder.size() - 1; i++) {
		if (abs(start - sort_cylinder[i]) <= abs(start - sort_cylinder[next_pos]) && abs(start - sort_cylinder[next_pos]) < abs(start - sort_cylinder[i + 1]))
			next_pos = i + 1;
	}
	for (int i = next_pos; i < sort_cylinder.size(); i++) {
		total += abs(start - sort_cylinder[i]);
		start = sort_cylinder[i];
	}
	
	return total + (sort_cylinder.end() - sort_cylinder.begin());
}

int clook(std::vector<int> cylinderRequest, int startPosition) {
	std::vector<int> sort_cylinder = cylinderRequest;
	std::sort(sort_cylinder.begin(), sort_cylinder.end());
	int start = startPosition;
	int next_pos = 0;
	for (int i = 0; i < sort_cylinder.size() - 1; i++) {
		if (abs(start - sort_cylinder[i]) <= abs(start - sort_cylinder[next_pos]) && abs(start - sort_cylinder[next_pos]) < abs(start - sort_cylinder[i + 1]))
			next_pos = i + 1;
	}
	return abs(sort_cylinder[sort_cylinder.size() - 1] - sort_cylinder[next_pos]) + (sort_cylinder[next_pos - 1] - sort_cylinder[0]) + 5000;
}

int main(int argc, char** argv) {
	int startPosition = atoi(argv[1]);
	int seed = 0;
	int num = 0;
	if (argv[2] == NULL) {
		seed = 255;
		num = 1000;
	} else {
		seed = atoi(argv[2]);
		num = atoi(argv[3]);
	}
	std::vector<int> cylinderRequest = cylinderGenerator(seed, num);
	std::cout << "FCFS   with start position " << startPosition << ", seed " << seed << " in total " << num << " blocks" << std::endl;
	std::cout << "total cylinder : " << fcfs(cylinderRequest, startPosition) << std::endl;
	std::cout << "SSTF   with start position " << startPosition << ", seed " << seed << " in total " << num << " blocks" << std::endl;
	std::cout << "total cylinder : " << sstf(cylinderRequest, startPosition) << std::endl;
	std::cout << "SCAN   with start position " << startPosition << ", seed " << seed << " in total " << num << " blocks" << std::endl;
	std::cout << "total cylinder : " << scan(cylinderRequest, startPosition) << std::endl;
	std::cout << "C-SCAN with start position " << startPosition << ", seed " << seed << " in total " << num << " blocks" << std::endl;
	std::cout << "total cylinder : " << cscan(cylinderRequest, startPosition) << std::endl;
	std::cout << "LOOK   with start position " << startPosition << ", seed " << seed << " in total " << num << " blocks" << std::endl;
	std::cout << "total cylinder : " << look(cylinderRequest, startPosition) << std::endl;
	std::cout << "C-LOOK with start position " << startPosition << ", seed " << seed << " in total " << num << " blocks" << std::endl;
	std::cout << "total cylinder : " << clook(cylinderRequest, startPosition) << std::endl;

}